package io.antmedia.android;

import java.io.File;

public class HandleEventPojo {

    String authcode;
    String deviceid;
    String epochStr;
    String eventType;
    String latlng;
    String fileName;
    File video_file;

    public HandleEventPojo(String authcode, String deviceid, String epochStr, String eventType, String latlng, String fileName, File video_file) {
        this.authcode = authcode;
        this.deviceid = deviceid;
        this.epochStr = epochStr;
        this.eventType = eventType;
        this.latlng = latlng;
        this.fileName = fileName;
        this.video_file = video_file;
    }

    public String getAuthcode() {
        return authcode;
    }

    public void setAuthcode(String authcode) {
        this.authcode = authcode;
    }

    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    public String getEpochStr() {
        return epochStr;
    }

    public void setEpochStr(String epochStr) {
        this.epochStr = epochStr;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getLatlng() {
        return latlng;
    }

    public void setLatlng(String latlng) {
        this.latlng = latlng;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public File getVideo_file() {
        return video_file;
    }

    public void setVideo_file(File video_file) {
        this.video_file = video_file;
    }
}
