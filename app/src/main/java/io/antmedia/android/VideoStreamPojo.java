package io.antmedia.android;

import java.io.File;

public class VideoStreamPojo {

    String startTime;
    String endTime;
    File videoFile;

    public VideoStreamPojo() {
        this.startTime = "";
        this.endTime = "";
        this.videoFile = null;
    }

    public VideoStreamPojo(String startTime, String endTime, File videoFile) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.videoFile = videoFile;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public File getVideoFile() {
        return videoFile;
    }

    public void setVideoFile(File videoFile) {
        this.videoFile = videoFile;
    }
}
