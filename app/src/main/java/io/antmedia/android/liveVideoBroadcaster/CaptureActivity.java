package io.antmedia.android.liveVideoBroadcaster;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.location.Location;
import android.location.LocationListener;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.ScheduledThreadPoolExecutor;

import io.antmedia.android.AccelerationPojo;
import io.antmedia.android.App;
import io.antmedia.android.HandleEventPojo;
import io.antmedia.android.MainActivity;
import io.antmedia.android.MyService;
import io.antmedia.android.Util;
import io.antmedia.android.ffmpegUtil;
import io.antmedia.android.model.LogoutResponse;
import io.antmedia.android.model.SendVideoResponse;
import io.antmedia.android.rest.ApiClient;
import io.antmedia.android.rest.ApiInterface;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static io.antmedia.android.Util.dateTimeFormat;
import static io.antmedia.android.Util.fileNameFormat;

public class CaptureActivity extends Activity implements SurfaceHolder.Callback, LocationListener {

    public static MediaRecorder recorder;
    static SurfaceHolder holder;
    public static boolean recording = false;
    private static final int REQUEST_PERMISSIONS = 10;
    public String[] permissions;
    public ArrayList<String> permissionsList;
    public boolean permissionsGranted = false;
    public static Activity activity;
    public Button trigger_hardbraking, trigger_excess_acceleration;
    public Switch start_stop_cam, toggle_cam;
    public boolean isSurfaceCreated = false;
    public static ScheduledThreadPoolExecutor scheduledThreadPoolExecutor;
    List<Location> speedDataQueueList = new ArrayList<Location>();
    int speedDataArrayLimit = 500;
    public ImageView logout;
    // Limits for excessive acceleration and deceleration in m/s2
    double excessiveaccelerationlimit = 4.4145;
    double excessivedecelerationlimit = 5.21892;
    public Boolean excessiveaccelerationOccured = false;
    public Boolean excessivedecelerationOccured = false;
    public TextView notification_view, driving_speed_value, stream_name_text;
    public Button broadcast;
    public static boolean isHardBrakeExcessAccelEvent = false, isHardBrakeExcessAccelEventUpload = false;
    public static boolean isCaptureActivityRunning = false;
    public static SharedPreferences pref;
    public static SharedPreferences.Editor editor;
    public static String TAG = "CaptureActivity";
    public static TextView stream_live_status, uploading_video;
    private static final int SENSOR_ORIENTATION_DEFAULT_DEGREES = 90;
    private static final int SENSOR_ORIENTATION_INVERSE_DEGREES = 270;
    private static final SparseIntArray DEFAULT_ORIENTATIONS = new SparseIntArray();
    private static final SparseIntArray INVERSE_ORIENTATIONS = new SparseIntArray();

    private static final int mCameraType = 0;
    static {
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_0, 90);
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_90, 0);
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_180, 270);
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    static {
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_0, 270);
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_90, 180);
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_180, 90);
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_270, 0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_capture);
        activity = this;

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        driving_speed_value = (TextView) findViewById(R.id.driving_speed_value);
        notification_view = (TextView) findViewById(R.id.notification_view);
        stream_name_text = (TextView) findViewById(R.id.stream_name_text);
        broadcast = (Button) findViewById(R.id.broadcasting);
        trigger_hardbraking = (Button) findViewById(R.id.trigger_hardbraking);
        trigger_excess_acceleration = (Button) findViewById(R.id.trigger_excess_acceleration);
        logout = (ImageView) findViewById(R.id.logout);

        stream_live_status = (TextView) findViewById(R.id.stream_live_status);
        uploading_video = (TextView) findViewById(R.id.uploading_video);
        pref = activity.getSharedPreferences("DataStore", MODE_PRIVATE);
        editor = pref.edit();

        start_stop_cam = (Switch) findViewById(R.id.start_stop_cam);
        start_stop_cam.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isSurfaceCreated){
                    if(isChecked) {     // Start service
                        if(!recording) {
                            if(!MyService.isRunning) {
                                /* Work in progress
                                Intent intent = new Intent(CaptureActivity.this, MyIntentService.class);
                                */
                                Intent intent = new Intent(CaptureActivity.this, MyService.class);
                                startService(intent);
                            }
                            startRecording();
                        } else {
                            Toast.makeText(getApplicationContext(), "Already recording", Toast.LENGTH_SHORT).show();
                        }
                    } else {            // Stop service
                        if (recording) {
                            if(MyService.isRunning) {
                                /* Work in progress
                                Intent intent = new Intent(CaptureActivity.this, MyIntentService.class);
                                */
                                Intent intent = new Intent(CaptureActivity.this, MyService.class);
                                stopService(intent);
                            }
                            stopRecording();
                        }
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Camera not loaded. Please wait", Toast.LENGTH_SHORT).show();
                }
            }
        });

        toggle_cam = (Switch) findViewById(R.id.toggle_cam);
        toggle_cam.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // toggleCam();
                /* if(isChecked) {
                    Toast.makeText(activity, "Front cam active", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(activity, "Back cam active", Toast.LENGTH_SHORT).show();
                } */
            }
        });

        broadcast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isHardBrakeExcessAccelEvent && !isHardBrakeExcessAccelEventUpload) {
                    if (recording) {
                        stopRecording();
                        if (MyService.isRunning) {
                            Intent intent = new Intent(CaptureActivity.this, MyService.class);
                            stopService(intent);
                        }
                    }
                    Toast.makeText(getApplicationContext(), "Switching to broadcasting mode", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(CaptureActivity.this, LiveVideoBroadcasterActivity.class);
                    startActivity(i);
                    // finish();
                    android.os.Process.killProcess(android.os.Process.myPid());
                } else {
                    Toast.makeText(activity, "Restricted during video upload", Toast.LENGTH_SHORT).show();
                }
            }
        });

        permissions = new String[]{};
        permissionsList = new ArrayList<>();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(Manifest.permission.CAMERA);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(Manifest.permission.RECORD_AUDIO);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        permissions = permissionsList.toArray(permissions);

        if (permissions.length > 0) {
            ActivityCompat.requestPermissions(activity, permissions, REQUEST_PERMISSIONS);
        } else {
            permissionsGranted = true;
            recorder = new MediaRecorder();
            initRecorder();


            SurfaceView cameraView = (SurfaceView) findViewById(R.id.CameraView);
            holder = cameraView.getHolder();
            holder.addCallback(this);
        }

        trigger_hardbraking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isHardBrakeExcessAccelEvent && !isHardBrakeExcessAccelEventUpload) {
                    recordEvent("1");
                } else {
                    String curtime = DateFormat.getDateTimeInstance().format(new Date());
                    Toast.makeText(activity, "Hard Braking detected at "+ curtime, Toast.LENGTH_LONG).show();
                }
            }
        });

        trigger_excess_acceleration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isHardBrakeExcessAccelEvent && !isHardBrakeExcessAccelEventUpload){
                    recordEvent("2");
                } else {
                    String curtime = DateFormat.getDateTimeInstance().format(new Date());
                    Toast.makeText(activity, "Excess acceleration detected at "+ curtime, Toast.LENGTH_LONG).show();
                }
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String authcode = pref.getString("authcode", "");
                logoutApi(authcode);
            }
        });
    }

    public void recordEvent(final String event){
        String eventFull = Util.eventFull(event);
        String eventShort = Util.eventShort(event);
        String curtime = DateFormat.getDateTimeInstance().format(new Date());
        Toast.makeText(activity, eventFull + " detected at "+ curtime, Toast.LENGTH_LONG).show();
        if(recording) {
            Calendar cal = new GregorianCalendar();
            cal.add(Calendar.SECOND, -10);
            final String startTime = dateTimeFormat.format(cal.getTime());
            final String fileName = eventShort+fileNameFormat.format(new Date()) + ".mp4";
            notification_view.setText("Recording "+ eventFull);
            notification_view.setVisibility(View.VISIBLE);
            isHardBrakeExcessAccelEvent = true;
            new CountDownTimer(10000, 1000) {
                public void onTick(long millisUntilFinished) {
                    // startRecording();
                    //here you can have your logic to set text to edittext
                }

                public void onFinish() {
                    String endTime = dateTimeFormat.format(new Date());
                    isHardBrakeExcessAccelEvent = false;
                    isHardBrakeExcessAccelEventUpload = true;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            start_stop_cam.setChecked(false);
                            stream_live_status.setVisibility(View.GONE);
                            uploading_video.setVisibility(View.VISIBLE);
                            notification_view.setText("");
                            notification_view.setVisibility(View.GONE);
                        }
                    });
                    createhardBrakeOrExcessiveAccelRequest(event, startTime, endTime, fileName);
                }
            }.start();
        } else {
            Toast.makeText(activity, eventFull + ": Not recording event. Camera Off", Toast.LENGTH_SHORT).show();
        }
    }

    public void logoutApi(String authcode){
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            Call<LogoutResponse> call = apiService.getLogoutResponse(authcode);  //apiService.getLoginCheckResponse(RTMP_BASE_URL);
            call.enqueue(new Callback<LogoutResponse>() {
                @Override
                public void onResponse(Call<LogoutResponse> call, Response<LogoutResponse> response) {
                    int statusCode = response.code();

                    if (statusCode == 200) {
                        if (response.body().getStatus().equalsIgnoreCase("Success")) {
                            editor.putString("authcode", "");
                            editor.commit();

                            if(MyService.isRunning){
                                Intent intent = new Intent(CaptureActivity.this, MyService.class);
                                stopService(intent);
                            }

                            Toast.makeText(activity, "Logging Out",  Toast.LENGTH_SHORT).show();
                            Log.d("Logout", response.body().getMessage());
                            Intent i = new Intent(activity, MainActivity.class);
                            activity.startActivity(i);
                            finish();
                        }
                        else {
                            Log.d("Logout", response.body().getMessage());
                            Toast.makeText(activity, "Logout failed", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        Toast.makeText(activity,"Something went wrong", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<LogoutResponse> call, Throwable t) {
                    // Log error here since request failed
                    Toast.makeText(activity,"Please check your internet connection", Toast.LENGTH_SHORT).show();
                }
            });
    }

    public void startRecording(){
        try {
            String auth = pref.getString("authcode", "");
            stream_name_text.setText(auth);
            initRecorder();
            prepareRecorder();
            recording = true;
            recorder.start();
            stream_live_status.setVisibility(View.VISIBLE);
            start_stop_cam.setText("Stop Recording");
            Toast.makeText(activity, "Recording started", Toast.LENGTH_SHORT).show();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public void stopRecording(){
        try {
            stream_name_text.setText("");
            // recorder.stop();
            recording = false;
            stream_live_status.setVisibility(View.GONE);
            releaseMediaRecorder();
            // scheduledThreadPoolExecutor.shutdown();
            start_stop_cam.setText("Start Recording");
            Toast.makeText(activity, "Recording stopped", Toast.LENGTH_SHORT).show();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void releaseMediaRecorder(){
        if (recorder != null) {
            // clear recorder configuration
            recorder.reset();
            // release the recorder object
            recorder.release();
            recorder = null;
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void initRecorder() {
        recorder = new MediaRecorder();
        try {
            if (recorder != null) {
                recorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
                recorder.setVideoSource(MediaRecorder.VideoSource.DEFAULT);
                CamcorderProfile cpHigh = CamcorderProfile.get(CamcorderProfile.QUALITY_480P);
                // int cameraType = 1;
                // Camera camera = Camera.open(cameraType);
                // recorder.setCamera(camera);
                cpHigh.videoCodec = MediaRecorder.VideoEncoder.H264;
                recorder.setProfile(cpHigh);
                recorder.setOutputFile(getStreamingPath().toString());
                // recorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
                int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();

                CameraManager manager = (CameraManager) activity.getSystemService(activity.CAMERA_SERVICE);
                int mSensorOrientation = 0;
                try {
                    String cameraId = manager.getCameraIdList()[0];

                    // Choose the sizes for camera preview and video recording
                    CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);
                    StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                    mSensorOrientation = characteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
                } catch (Exception e){
                    e.printStackTrace();
                }

                switch (mSensorOrientation) {
                    case SENSOR_ORIENTATION_DEFAULT_DEGREES:
                        recorder.setOrientationHint(DEFAULT_ORIENTATIONS.get(rotation));
                        break;
                    case SENSOR_ORIENTATION_INVERSE_DEGREES:
                        recorder.setOrientationHint(INVERSE_ORIENTATIONS.get(rotation));
                        break;
                }
            } else {
                Log.d("initRecorder", "recorder not initialized");
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        // recorder.setMaxDuration(50000); // 50 seconds
        // recorder.setMaxFileSize(5000000); // Approximately 5 megabytes
    }

    private CamcorderProfile getCameraProfile(){
        CamcorderProfile profile = CamcorderProfile.get(Camera.CameraInfo.CAMERA_FACING_FRONT,CamcorderProfile.QUALITY_LOW);
        profile.fileFormat = MediaRecorder.OutputFormat.MPEG_4;
        profile.videoCodec = MediaRecorder.VideoEncoder.MPEG_4_SP;
        profile.videoFrameHeight = 240;
        profile.videoFrameWidth = 320;
        profile.videoBitRate = 15;

        return profile;
    }

    public static File getStreamingPath(){
        File folder = getFolderPath();
        File video_file = new File(folder, "streaming.mp4");

        return video_file;
    }

    public File getSavedPath(){
        File folder = getFolderPath();
        File video_file = new File(folder, "saved.mp4");

        return video_file;
    }

    public static File getFolderPath(){
        File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).toString());
        if(!folder.exists()){
            folder.mkdir();
        }
        return folder;
    }

    public static void prepareRecorder() {
        if(recorder != null) {
            recorder.setPreviewDisplay(holder.getSurface());

            try {
                recorder.prepare();
            } catch (IllegalStateException e) {
                e.printStackTrace();
                releaseMediaRecorder();
            } catch (IOException e) {
                e.printStackTrace();
                releaseMediaRecorder();
            }
        } else {
            Log.d("prepareRecorder", "Recorder is null");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        isCaptureActivityRunning = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isCaptureActivityRunning = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isSurfaceCreated = false;
        isCaptureActivityRunning = false;
        if(recording){
            if(MyService.isRunning){
                Intent intent = new Intent(CaptureActivity.this, MyService.class);
                stopService(intent);
            }
        }
        releaseMediaRecorder();
    }

    @Override
    protected void onStop() {
        super.onStop();
        isSurfaceCreated = false;
        isCaptureActivityRunning = false;
        if(recording){
            if(MyService.isRunning){
                Intent intent = new Intent(CaptureActivity.this, MyService.class);
                stopService(intent);
            }
        }
        releaseMediaRecorder();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isSurfaceCreated = false;
        if(recording){
            if(MyService.isRunning){
                Intent intent = new Intent(CaptureActivity.this, MyService.class);
                stopService(intent);
            }
        }
        releaseMediaRecorder();
    }

    /* public void onClick(View v) {
        if (recording) {r
            recorder.stop();
            recording = false;

            // Let's initRecorder so we can record again
            initRecorder();
            prepareRecorder();
        } else {
            recording = true;
            recorder.start();
        }
    } */

    @Override
    public void onLocationChanged(Location location) {

        if (location!=null){
            if(speedDataQueueList.size() >= speedDataArrayLimit) {
                speedDataQueueList.remove(0);
            }  else {
                speedDataQueueList.add(location);
            }

            int speed=(int) ((location.getSpeed() * 3600) / 1000);
            driving_speed_value.setText("Driving speed: " + speed +" km/h");
            //driving_speed_value.setText("Diving speed: " + location.getSpeed() +" m/s");

            if(speedDataQueueList.size() > 1) {
                int range = 2;
                if(speedDataQueueList.size() >= 5){
                    range = 5;
                } else {
                    range = speedDataQueueList.size();
                }

                excessiveaccelerationOccured = false;
                excessivedecelerationOccured = false;

                Location curspeedValue = speedDataQueueList.get(speedDataQueueList.size() - 1); // latest value
                // Checking till last 5 values to see if excessive acceleration/ hard breaking has occured at any point
                for (int i = speedDataQueueList.size() - 2; i >= (speedDataQueueList.size() - range); i--) {
                    Location initialValue = speedDataQueueList.get(i);
                    double initialvelocity = initialValue.getSpeed();
                    double finalvelocity = curspeedValue.getSpeed();
                    long initialtime = (initialValue.getTime() / 1000);
                    long finaltime = (curspeedValue.getTime() / 1000);

                    AccelerationPojo accelerationPojo = new AccelerationPojo(initialvelocity, finalvelocity, initialtime, finaltime);
                    double curacceleration = accelerationPojo.calculateAcceleration();
                    double deltat = (finaltime - initialtime);
                    if(curacceleration < 0){    // deceleration
                        // if(Math.abs(curacceleration) >= (excessivedecelerationlimit)){
                        if(Math.abs(curacceleration) >= (excessivedecelerationlimit)){
                            excessivedecelerationOccured = true;
                            if(notification_view.getVisibility() == View.GONE){
                                notification_view.setText("Hard braking detected");
                                notification_view.setVisibility(View.VISIBLE);
                                if(!isHardBrakeExcessAccelEvent && !isHardBrakeExcessAccelEventUpload) {
                                    recordEvent("1");
                                }
                                // store in db with timestamp, notify server
                            } else {
                                notification_view.setText("Hard braking detected");
                            }
                            break;
                        }
                    } else {    // acceleration
                        // if(Math.abs(curacceleration) >= (excessiveaccelerationlimit)){
                        if(Math.abs(curacceleration) >= (excessiveaccelerationlimit)){
                            excessiveaccelerationOccured = true;
                            if(notification_view.getVisibility() == View.GONE){
                                notification_view.setText("Excessive acceleration detected");
                                notification_view.setVisibility(View.VISIBLE);
                                if(!isHardBrakeExcessAccelEvent && !isHardBrakeExcessAccelEventUpload) {
                                    recordEvent("2");
                                }
                                // store in db with timestamp, notify server
                            } else {
                                notification_view.setText("Excessive acceleration detected");
                            }
                            break;
                        }
                    }
                }

                if(!excessivedecelerationOccured && !excessiveaccelerationOccured){
                    notification_view.setVisibility(View.GONE);
                }
            }
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(getApplicationContext(), "Please turn on your GPS", Toast.LENGTH_SHORT).show();
    }

    public void surfaceCreated(SurfaceHolder holder) {
        prepareRecorder();
        isSurfaceCreated = true;
        // startRecording();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.d(TAG, "Surface Changed");
    }

    Runnable cropVideo = new Runnable() {
        @Override
        public void run() {
            try {
                stopRecording();

                // Replace saved video with streaming video
                replaceSavedWithStreamingVideo();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public void remux(){
        // savedFile.delete();
        String faststart_cmd = getResources().getString(R.string.ffmpeg_faststart_remux);
        String videoIn = getFolderPath() + "/streaming.mp4";
        String videoOut = getFolderPath() + "/streamingnew.mp4";
        faststart_cmd = faststart_cmd.replace("video_in", videoIn);
        faststart_cmd = faststart_cmd.replace("video_out", videoOut);
        String[] faststart = ffmpegUtil.convertToCommandStringArray(faststart_cmd);
        executeFastStartRemuxCommand(faststart);
    }

    public void replaceSavedWithStreamingVideo(){
        String copy_cmd = getResources().getString(R.string.ffmpeg_copy_cmd);
        String videoIn = getFolderPath() + "/streamingnew.mp4";
        String videoOut = getFolderPath() + "/saved.mp4";
        copy_cmd = copy_cmd.replace("video_in", videoIn);
        copy_cmd = copy_cmd.replace("video_out", videoOut);
        String[] cmd = ffmpegUtil.convertToCommandStringArray(copy_cmd);
        executeSaveVideoCommand(cmd);
        // streamingFile.delete();
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        if(recorder != null) {
            if (recording) {
                recorder.stop();
                recording = false;
            }
            recorder.release();
        }
        finish();
    }

    public void executeFastStartRemuxCommand(String[] cmd){
        try {
            // to execute "ffmpeg -version" command you just need to pass "-version"
            App.getFfmpeg().execute(cmd, new ExecuteBinaryResponseHandler() {

                @Override
                public void onStart() {
                    Log.d("ffmpeg", "onStart");
                }

                @Override
                public void onProgress(String message) {
                    Log.d("ffmpeg", "onProgress: "+message);
                }

                @Override
                public void onFailure(String message) {
                    Log.d("ffmpeg", "onFailure: "+message);
                }

                @Override
                public void onSuccess(String message) {
                    Log.d("ffmpeg", "onSuccess: "+message);
                }

                @Override
                public void onFinish() {
                    Log.d("ffmpeg", "onFinish");
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            // Handle if FFmpeg is already running
            e.printStackTrace();
        }
    }

    public void executeSaveVideoCommand(String[] cmd){
        try {
            // to execute "ffmpeg -version" command you just need to pass "-version"
            App.getFfmpeg().execute(cmd, new ExecuteBinaryResponseHandler() {

                @Override
                public void onStart() {
                    Log.d("ffmpeg", "onStart");
                }

                @Override
                public void onProgress(String message) {
                    Log.d("ffmpeg", "onProgress: "+message);
                }

                @Override
                public void onFailure(String message) {
                    Log.d("ffmpeg", "onFailure: "+message);
                }

                @Override
                public void onSuccess(String message) {
                    Log.d("ffmpeg", "onSuccess: "+message);
                }

                @Override
                public void onFinish() {
                    Log.d("ffmpeg", "onFinish");
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            // Handle if FFmpeg is already running
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_PERMISSIONS: {
                permissionsGranted = true;
                for(int result:grantResults) {
                    if(result != PackageManager.PERMISSION_GRANTED){
                        permissionsGranted = false;
                    }
                }

                if(!permissionsGranted){
                    Log.d("Permission", "Permission denied!");
                    Toast.makeText(activity, "Permission denied", Toast.LENGTH_SHORT).show();
                    finish();
                    // appWontWorkWithoutPermissionsAlert();
                } else {
                    initRecorder();

                    SurfaceView cameraView = (SurfaceView) findViewById(R.id.CameraView);
                    holder = cameraView.getHolder();
                    holder.addCallback(this);
                }
            }
        }
    }

    public void createhardBrakeOrExcessiveAccelRequest(final String eventType, final String startTime, final String endTime, final String fileName){

        new AsyncTask<Void, Void, String>() {
            @Override
            protected void onPreExecute() {
                Log.d("Video file", "Video clipping and merging");
            }

            @Override
            protected String doInBackground(Void... voids) {

                // File streaming_video = MyIntentService.getStreamingVideo();
                // File saved_video = MyIntentService.getSavedVideo();

                // Work in Progress; Video splitting, merging
                /*
                EventVideoCreationPojo eventVideoCreationPojo = Util.createEventVideo(startTime, endTime, eventTypeStr);

                // Create video split(s) based on VideostreamPojo values
                String cut_cmd = activity.getResources().getString(R.string.ffmpeg_cut_video);
                if(eventVideoCreationPojo.getStartVideo().getVideoFile() != null) {
                    // chunk1
                    String videoIn = eventVideoCreationPojo.getStartVideo().getVideoFile().getAbsolutePath();
                    String videoOut = getFolderPath() + "/chunk1.mp4";
                    String startTime = eventVideoCreationPojo.getStartTime();
                    String duration = eventVideoCreationPojo.getStartDuration();
                    cut_cmd = cut_cmd.replace("video_in", videoIn);
                    cut_cmd = cut_cmd.replace("video_out", videoOut);
                    cut_cmd = cut_cmd.replace("[start]", startTime);
                    cut_cmd = cut_cmd.replace("[duration]", duration);
                    String[] cut_video = ffmpegUtil.convertToCommandStringArray(cut_cmd);
                    executeFFMPEGCommand(cut_video);
                }

                if(eventVideoCreationPojo.getEndVideo().getVideoFile() != null) {
                    // chunk2
                    String videoIn = eventVideoCreationPojo.getEndVideo().getVideoFile().getAbsolutePath();
                    String videoOut = getFolderPath() + "/chunk2.mp4";
                    String startTime = eventVideoCreationPojo.getEndTime();
                    String duration = eventVideoCreationPojo.getEndDuration();
                    cut_cmd = cut_cmd.replace("video_in", videoIn);
                    cut_cmd = cut_cmd.replace("video_out", videoOut);
                    cut_cmd = cut_cmd.replace("[start]", startTime);
                    cut_cmd = cut_cmd.replace("[duration]", duration);
                    String[] cut_video = ffmpegUtil.convertToCommandStringArray(cut_cmd);
                    executeFFMPEGCommand(cut_video);
                }

                // Merge split(s) to create final video, Save as video_file
                String merge_cmd = activity.getResources().getString(R.string.ffmpeg_merge_video);
                String video1 = getFolderPath() + "/chunk1.mp4";
                String video2 = getFolderPath() + "/chunk2.mp4";
                String videoOut = getFolderPath() + "/final_video.mp4";
                merge_cmd = merge_cmd.replace("video1", video1);
                merge_cmd = merge_cmd.replace("video2", video2);
                merge_cmd = merge_cmd.replace("video_out", videoOut);
                String[] merge_video = ffmpegUtil.convertToCommandStringArray(merge_cmd);
                executeFFMPEGCommand(merge_video);
                File video_file = new File(getFolderPath(), "final_video.mp4");
                */

                createVideoFileCopy(getStreamingPath(), getSavedPath());
                return "";
            }

            @Override
            protected void onPostExecute(String abc) {
                pref = getApplicationContext().getSharedPreferences("DataStore", MODE_PRIVATE);
                String authcode = pref.getString("authcode", "");
                String deviceid = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
                long epoch = (System.currentTimeMillis() / 1000);
                String epochStr = String.valueOf(epoch);
                String eventTypeStr = eventType;
                String latlng = "";
                if(speedDataQueueList != null){
                    if(speedDataQueueList.size() > 0){
                        Location location = speedDataQueueList.get(speedDataQueueList.size() - 1);
                        String lat = String.valueOf(location.getLatitude());
                        String lng = String.valueOf(location.getLongitude());
                        latlng = lat + "," + lng;
                    }
                }
                File video_file = getSavedPath();

                HandleEventPojo handleEventPojo = new HandleEventPojo(authcode, deviceid, epochStr, eventTypeStr, latlng, fileName, video_file);
                hardbrakeOrExcessiveAccelApi(handleEventPojo);
            }
        }.execute();
    }


    public void hardbrakeOrExcessiveAccelApi(HandleEventPojo handleEventPojo) {

        File video_file = handleEventPojo.getVideo_file();
        String authcode = handleEventPojo.getAuthcode();
        String deviceId = handleEventPojo.getDeviceid();
        String latlng = handleEventPojo.getLatlng();
        String eventType = handleEventPojo.getEventType();
        String epochStr = handleEventPojo.getEpochStr();
        String fileName = handleEventPojo.getFileName();

        // Parsing any Media type file
        RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), video_file);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("video", fileName, requestBody);
        RequestBody video = RequestBody.create(MediaType.parse("text/plain"), video_file.getName());

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        /* Call<SendVideoResponse> call = apiService.getSendVideoResponse(authcode, deviceId, latlng, eventType, epochStr, fileToUpload);
        call.enqueue(new Callback<SendVideoResponse>() {
            @Override
            public void onResponse(Call<SendVideoResponse> call, Response<SendVideoResponse> response) {
                int statusCode = response.code();
                isHardBrakeExcessAccelEventUpload = false;
                start_stop_cam.setChecked(true);
                uploading_video.setVisibility(View.GONE);

                if (statusCode == 200) {
                    if (response.body().getStatus().equalsIgnoreCase("Success")) {
                        Log.d("SendVideo", "Success");
                        Toast.makeText(activity, "Video uploaded", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Log.d("SendVideo", "Error");
                        Toast.makeText(activity, "Video upload failed", Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Log.d("SendVideo", "Request Failed");
                    Toast.makeText(activity, "Video upload failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                isHardBrakeExcessAccelEventUpload = false;
                uploading_video.setVisibility(View.GONE);
                Log.d("SendVideo", "SendVideo failure");
                if(!MyService.isRunning){
                    Intent intent = new Intent(CaptureActivity.this, MyService.class);
                    startService(intent);
                }
                startRecording();
                Toast.makeText(activity, "Video upload failed", Toast.LENGTH_SHORT).show();
            }
        }); */
    }

    public static File getChunk1(){
        File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).toString());
        if(folder.exists()) {
            File chunk1_video = new File(folder, "chunk1.mp4");
            return chunk1_video;
        }
        return null;
    }

    public static File getChunk2(){
        File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).toString());
        if(folder.exists()) {
            File chunk2_video = new File(folder, "chunk2.mp4");
            return chunk2_video;
        }
        return null;
    }

    public void executeFFMPEGCommand(String[] cmd){
        try {
            // to execute "ffmpeg -version" command you just need to pass "-version"
            App.getFfmpeg().execute(cmd, new ExecuteBinaryResponseHandler() {

                @Override
                public void onStart() {
                    Log.d("ffmpeg", "onStart");
                }

                @Override
                public void onProgress(String message) {
                    Log.d("ffmpeg", "onProgress: "+message);
                }

                @Override
                public void onFailure(String message) {
                    Log.d("ffmpeg", "onFailure: "+message);
                }

                @Override
                public void onSuccess(String message) {
                    Log.d("ffmpeg", "onSuccess: "+message);
                }

                @Override
                public void onFinish() {
                    Log.d("ffmpeg", "onFinish");
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            // Handle if FFmpeg is already running
            e.printStackTrace();
        }
    }

    public void createVideoFileCopy(File inputPath, File outputPath){
        try {
            FileInputStream inputFile = new FileInputStream(inputPath);
            FileOutputStream outputFile = null;
            if(!outputPath.exists()){
                File outFile = new File(getFolderPath(), "saved.mp4");
                outputFile = new FileOutputStream (outFile.getAbsolutePath());
            } else {
                outputFile = new FileOutputStream(outputPath);
            }

            // Transfer bytes from in to out
            byte[] buf = new byte[1024];
            int len;
            while ((len = inputFile.read(buf)) > 0) {
                outputFile.write(buf, 0, len);
            }
            inputFile.close();
            outputFile.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
