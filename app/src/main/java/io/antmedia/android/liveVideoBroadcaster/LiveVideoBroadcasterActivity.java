package io.antmedia.android.liveVideoBroadcaster;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.opengl.GLSurfaceView;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.io.File;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import io.antmedia.android.AccelerationPojo;
import io.antmedia.android.MainActivity;
import io.antmedia.android.MyService;
import io.antmedia.android.Util;
import io.antmedia.android.fcm.MYFirebaseMessagingService;
import io.antmedia.android.liveVideoBroadcaster.CaptureActivity;
import io.antmedia.android.broadcaster.ILiveVideoBroadcaster;
import io.antmedia.android.broadcaster.LiveVideoBroadcaster;
import io.antmedia.android.broadcaster.utils.Resolution;
import io.antmedia.android.model.LogoutResponse;
import io.antmedia.android.model.SendVideoResponse;
import io.antmedia.android.rest.ApiClient;
import io.antmedia.android.rest.ApiInterface;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static io.antmedia.android.MainActivity.RTMP_BASE_URL;
import static io.antmedia.android.MainActivity.myInstance;
import static io.antmedia.android.MainActivity.pref;
import static io.antmedia.android.Util.dateTimeFormat;

public class LiveVideoBroadcasterActivity extends AppCompatActivity implements LocationListener {

    private static final String TAG = LiveVideoBroadcasterActivity.class.getSimpleName();
    private static ViewGroup mRootView;
    private static boolean mIsRecording = false;
    private static TextView mStreamNameText;
    private static  Timer mTimer;
    private static long mElapsedTime;
    public static TimerHandler mTimerHandler;
    private ImageButton mSettingsButton;
    private ImageView logout;
    private CameraResolutionsFragment mCameraResolutionsDialog;
    private Intent mLiveVideoBroadcasterServiceIntent;
    private static TextView mStreamLiveStatus, trigger_hardbraking, trigger_excess_acceleration;
    private GLSurfaceView mGLView;
    private static ILiveVideoBroadcaster mLiveVideoBroadcaster;
    private static Button mBroadcastControlButton;
    public static Activity activity;
    public static LiveVideoBroadcasterActivity brdInstance;
    public static String streamNameRegisterd;
    public TextView notification_view, driving_speed_value;
    private ProgressBar lbSpinner;
    public static boolean isLiveBroadcastActivityRunning = false;
    public Integer SpeedLimit = 25;

    Handler mihndler = new Handler();
    int miDelay = 25*1000; //1 second=1000 milisecond, 25*1000 = 25seconds
    Runnable mirRunnable;

    public Button record;

    public Boolean excessiveaccelerationOccured = false;
    public Boolean excessivedecelerationOccured = false;

    // Limits for excessive acceleration and deceleration in m/s2 in one sec
    double excessiveaccelerationlimit = 4.4145;
    double excessivedecelerationlimit = 5.21892;

    // double excessiveaccelerationlimittest = 1.38889;
    // double excessivedecelerationlimittest = 1.38889;

    public boolean isVideoCaptured = false;

    // List<Location> speedDataQueueList = new ArrayList<Location>();
    List<Double> testSpeedDataQueueList = new ArrayList<Double>();
    List<Location> speedDataQueueList = new ArrayList<Location>();

    private final int VIDEO_REQUEST_CODE = 100;
    int speedDataArrayLimit = 500;

    public static SharedPreferences pref;
    public static SharedPreferences.Editor editor;

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            LiveVideoBroadcaster.LocalBinder binder = (LiveVideoBroadcaster.LocalBinder) service;
            if (mLiveVideoBroadcaster == null) {
                mLiveVideoBroadcaster = binder.getService();
                mLiveVideoBroadcaster.init(LiveVideoBroadcasterActivity.this, mGLView);
                mLiveVideoBroadcaster.setAdaptiveStreaming(true);
            }
            mLiveVideoBroadcaster.openCamera(Camera.CameraInfo.CAMERA_FACING_BACK);
            notificationCustomAction();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mLiveVideoBroadcaster = null;
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        brdInstance = this;
        activity = this;
        mLiveVideoBroadcasterServiceIntent = new Intent(this, LiveVideoBroadcaster.class);

        startService(mLiveVideoBroadcasterServiceIntent);

        setContentView(R.layout.frameexample);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        // setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        mTimerHandler = new TimerHandler();
        mStreamNameText = (TextView) findViewById(R.id.stream_name_edit_text);

        mRootView = (ViewGroup) findViewById(R.id.root_layout);
        mSettingsButton = (ImageButton) findViewById(R.id.settings_button);
        mStreamLiveStatus = (TextView) findViewById(R.id.stream_live_status);
        driving_speed_value = (TextView) findViewById(R.id.driving_speed_value);
        notification_view = (TextView) findViewById(R.id.notification_view);
        logout = (ImageView) findViewById(R.id.logout);
        mBroadcastControlButton = (Button) findViewById(R.id.broadcasting);
        record = (Button) findViewById(R.id.live_record);

        pref = activity.getSharedPreferences("DataStore", MODE_PRIVATE);
        editor = pref.edit();

        mGLView = (GLSurfaceView) findViewById(R.id.cameraPreview_surfaceView);
        if (mGLView != null) {
            mGLView.setEGLContextClientVersion(2);     // select GLES 2.0
        }

        mBroadcastControlButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleBroadcasting(mGLView, true);
            }
        });

        record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(activity, "Switching to recording mode", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(activity, CaptureActivity.class);
                startActivity(i);
                // finish();
                android.os.Process.killProcess(android.os.Process.myPid());
            }
        });

        trigger_hardbraking = (Button) findViewById(R.id.trigger_hardbraking);
        trigger_excess_acceleration = (Button) findViewById(R.id.trigger_excess_acceleration);

        trigger_hardbraking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String curtime = dateTimeFormat.format(new Date());
                Toast.makeText(activity, "Hard Braking recorded at "+ curtime, Toast.LENGTH_LONG).show();
                createhardBrakeOrExcessiveAccelRequest("1");
            }
        });

        trigger_excess_acceleration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String curtime = dateTimeFormat.format(new Date());
                Toast.makeText(activity, "Excess Accel. recorded at "+ curtime, Toast.LENGTH_LONG).show();
                createhardBrakeOrExcessiveAccelRequest("2");
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String authcode = pref.getString("authcode", "");
                logoutApi(authcode);
            }
        });
    }

    public void logoutApi(String authcode){
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<LogoutResponse> call = apiService.getLogoutResponse(authcode);  //apiService.getLoginCheckResponse(RTMP_BASE_URL);
        call.enqueue(new Callback<LogoutResponse>() {
            @Override
            public void onResponse(Call<LogoutResponse> call, Response<LogoutResponse> response) {
                int statusCode = response.code();

                if (statusCode == 200) {
                    if (response.body().getStatus().equalsIgnoreCase("Success")) {
                        Toast.makeText(activity, "Logging Out",  Toast.LENGTH_SHORT).show();
                        editor.putString("authcode", "");
                        editor.commit();

                        Log.d("Logout", response.body().getMessage());
                        Intent i = new Intent(activity, MainActivity.class);
                        activity.startActivity(i);
                        activity.finish();
                    }
                    else {
                        Log.d("Logout", response.body().getMessage());
                        Toast.makeText(activity, "Logout failed", Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Toast.makeText(activity,"Something went wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LogoutResponse> call, Throwable t) {
                // Log error here since request failed
                Toast.makeText(activity,"Please check your internet connection", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        bindService(mLiveVideoBroadcasterServiceIntent, mConnection, 0);
        if (!this.isLocationEnabled(this)) {
                //show dialog if Location Services is not enabled
                android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
                builder.setTitle("GPS not turned on");  // GPS not found
                builder.setMessage("Please turn on GPS"); // Want to enable?
                builder.setCancelable(false);
                builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                        myInstance.startActivity(intent);
                    }
                });

                builder.create().show();
        } else {
            LocationManager lm = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
                this.onLocationChanged(null);
            }
        }
    }

    public void changeCamera(View v) {
        if (mLiveVideoBroadcaster != null) {
            mLiveVideoBroadcaster.changeCamera();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        isLiveBroadcastActivityRunning = true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LiveVideoBroadcaster.PERMISSIONS_REQUEST: {
                if(mLiveVideoBroadcaster != null) {
                    if (mLiveVideoBroadcaster.isPermissionGranted()) {
                        mLiveVideoBroadcaster.openCamera(Camera.CameraInfo.CAMERA_FACING_BACK);

                        LocationManager lm = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
                        this.onLocationChanged(null);
                    }
                }
                return;
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        //hide dialog if visible not to create leaked window exception
        if (mCameraResolutionsDialog != null && mCameraResolutionsDialog.isVisible()) {
            mCameraResolutionsDialog.dismiss();
        }

        if(MyService.isRunning){
            Intent intent = new Intent(LiveVideoBroadcasterActivity.this, MyService.class);
            stopService(intent);
        }
        isLiveBroadcastActivityRunning = false;
        if(mLiveVideoBroadcaster != null) {
            mLiveVideoBroadcaster.pause();
        }

        unbindService(mConnection);
    }

    @Override
    protected void onStop() {
        super.onStop();
        isLiveBroadcastActivityRunning = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isLiveBroadcastActivityRunning = false;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE || newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            if(mLiveVideoBroadcaster != null) {
                mLiveVideoBroadcaster.setDisplayOrientation();
            }
        }
    }

    public void showSetResolutionDialog(View v) {

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment fragmentDialog = getSupportFragmentManager().findFragmentByTag("dialog");
        if (fragmentDialog != null) {

            ft.remove(fragmentDialog);
        }

        if(mLiveVideoBroadcaster != null) {
            ArrayList<Resolution> sizeList = mLiveVideoBroadcaster.getPreviewSizeList();

            if (sizeList != null && sizeList.size() > 0) {
                mCameraResolutionsDialog = new CameraResolutionsFragment();

                mCameraResolutionsDialog.setCameraResolutions(sizeList, mLiveVideoBroadcaster.getPreviewSize());
                mCameraResolutionsDialog.show(ft, "resolutiton_dialog");
            } else {
                // Snackbar.make(mRootView, "No resolution available",Snackbar.LENGTH_LONG).show();
            }
        }
    }

    public void toggleBroadcasting(View v, boolean isBroadcasting) {

        if(isBroadcasting) {
            if (!mIsRecording) {
                if (mLiveVideoBroadcaster != null) {
                    if (!mLiveVideoBroadcaster.isConnected()) {
                        String streamName = mStreamNameText.getText().toString();

                        new AsyncTask<String, String, Boolean>() {
                            ContentLoadingProgressBar
                                    progressBar;

                            @Override
                            protected void onPreExecute() {
                                progressBar = new ContentLoadingProgressBar(LiveVideoBroadcasterActivity.this);
                                progressBar.show();
                            }

                            @Override
                            protected Boolean doInBackground(String... url) {
                                return mLiveVideoBroadcaster.startBroadcasting(url[0]);

                            }

                            @Override
                            protected void onPostExecute(Boolean result) {
                                progressBar.hide();
                                mIsRecording = result;
                                if (result) {
                                    mStreamLiveStatus.setVisibility(View.VISIBLE);

                                    mBroadcastControlButton.setText(R.string.stop_broadcasting);
                                    mSettingsButton.setVisibility(View.GONE);
                                    startTimer();//start the recording duration
                                } else {
                                    Snackbar.make(mRootView, R.string.stream_not_started, Snackbar.LENGTH_LONG).show();

                                    triggerStopRecording();
                                }
                            }
                        }.execute(RTMP_BASE_URL + streamName);
                    } else {
                        Snackbar.make(mRootView, R.string.streaming_not_finished, Snackbar.LENGTH_LONG).show();
                    }
                } else {
                    Snackbar.make(mRootView, R.string.oopps_shouldnt_happen, Snackbar.LENGTH_LONG).show();
                }
            }else {
                triggerStopRecording();
            }
        }
        else
        {
            triggerStopRecording();
        }
    }

    public static void startVideoRecording(){
           captureVideo();
    }

    // Capture video with front and back camera toggle
    public static void captureVideo()
    {
        isLiveBroadcastActivityRunning = false;
        activity.finish();
    }

    public File getFilePath(){

        File folder = getFolderPath();
        File video_file = new File(folder, "saved_video.mp4");

        return video_file;
    }

    public File getFolderPath(){
        File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).toString());
        if(!folder.exists()){
            folder.mkdir();
        }
        return folder;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == VIDEO_REQUEST_CODE){
            if(resultCode == RESULT_OK){
                Toast.makeText(getApplicationContext(), "Video successfully recorded!", Toast.LENGTH_SHORT).show();
                isVideoCaptured = true;
            } else {
                Toast.makeText(getApplicationContext(), "Video record failed!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public static void triggerStopRecording() {
        if (mIsRecording) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mBroadcastControlButton.setText(R.string.start_broadcasting);
                    mStreamLiveStatus.setVisibility(View.GONE);
                    mStreamLiveStatus.setText(R.string.live_indicator);
//                mSettingsButton.setVisibility(View.VISIBLE);
                    mStreamNameText.setText(streamNameRegisterd);
                }
            });

            stopTimer();

            if(mLiveVideoBroadcaster != null) {
                mLiveVideoBroadcaster.stopBroadcasting();
            }
        }

        mIsRecording = false;
        // startVideoRecording();
    }

    //This method starts a mTimer and updates the textview to show elapsed time for recording
    public static void startTimer() {

        if(mTimer == null) {
            mTimer = new Timer();
        }

        mElapsedTime = 0;
        mTimer.scheduleAtFixedRate(new TimerTask() {

            public void run() {
                mElapsedTime += 1; //increase every sec
                if(mTimerHandler != null) {
                    mTimerHandler.obtainMessage(TimerHandler.INCREASE_TIMER).sendToTarget();

                    if (mLiveVideoBroadcaster == null || !mLiveVideoBroadcaster.isConnected()) {
                        mTimerHandler.obtainMessage(TimerHandler.CONNECTION_LOST).sendToTarget();
                    }
                } else {
                    Log.d(TAG, "Failed to start Timer");
                }
            }
        }, 0, 1000);
    }

    public static void stopTimer()
    {
        if (mTimer != null) {
            mTimer.cancel();
        }
        mTimer = null;
        mElapsedTime = 0;
    }

    public void setResolution(Resolution size) {
        if(mLiveVideoBroadcaster != null) {
            mLiveVideoBroadcaster.setResolution(size);
        }
    }

    private class TimerHandler extends Handler {
        static final int CONNECTION_LOST = 2;
        static final int INCREASE_TIMER = 1;

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case INCREASE_TIMER:
                    mStreamLiveStatus.setText(getString(R.string.live_indicator) + " - " + getDurationString((int) mElapsedTime));
                    break;
                case CONNECTION_LOST:
                    triggerStopRecording();
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new AlertDialog.Builder(activity)
                                    .setMessage(activity.getResources().getString(R.string.broadcast_connection_lost))
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    }).show();
                        }
                    });

                    break;
            }
        }
    }

    public static String getDurationString(int seconds) {

        if(seconds < 0 || seconds > 2000000)//there is an codec problem and duration is not set correctly,so display meaningfull string
            seconds = 0;
        int hours = seconds / 3600;
        int minutes = (seconds % 3600) / 60;
        seconds = seconds % 60;

        if(hours == 0)
            return twoDigitString(minutes) + " : " + twoDigitString(seconds);
        else
            return twoDigitString(hours) + " : " + twoDigitString(minutes) + " : " + twoDigitString(seconds);
    }

    public static String twoDigitString(int number) {

        if (number == 0) {
            return "00";
        }

        if (number / 10 == 0) {
            return "0" + number;
        }

        return String.valueOf(number);
    }

    public static void notificationCustomAction() {

        final String streamName = pref.getString("streamName", "");
        String eventType = pref.getString("eventType", "");
        String cameraID = pref.getString("cameratype", "");

            if (!mIsRecording) {
                if (eventType.equals("3")) {
                    triggerStopRecording();
                }
                else {
                    if (mLiveVideoBroadcaster != null) {
                        if (!mLiveVideoBroadcaster.isConnected()) {
//                    String streamName = streamName; //mStreamNameText.getText().toString();

                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    streamNameRegisterd = streamName;
                                    mStreamNameText.setText(streamNameRegisterd);
                                    mStreamNameText.setEnabled(false);
                                }
                            });

                            if(cameraID != null) {
                                if (cameraID.equals("1")) {
                                    mLiveVideoBroadcaster.changeCameraWithID(1);
                                } else {
                                    mLiveVideoBroadcaster.changeCameraWithID(0);
                                }
                            }

                            new AsyncTask<String, String, Boolean>() {
                                ContentLoadingProgressBar
                                        progressBar;

                                @Override
                                protected void onPreExecute() {
                                    progressBar = new ContentLoadingProgressBar(activity);
                                    progressBar.show();
                                }

                                @Override
                                protected Boolean doInBackground(String... url) {
                                    return mLiveVideoBroadcaster.startBroadcasting(url[0]);

                                }

                                @Override
                                protected void onPostExecute(Boolean result) {
                                    progressBar.hide();
                                    mIsRecording = result;
                                    if (result) {
                                        mStreamLiveStatus.setVisibility(View.VISIBLE);

                                        mBroadcastControlButton.setText(R.string.stop_broadcasting);
//                                    mSettingsButton.setVisibility(View.GONE);
                                        startTimer();//start the recording duration
                                    } else {
                                        activity.runOnUiThread(new Runnable() {
                                                                   @Override
                                                                   public void run() {
                                                                       Toast.makeText(activity, activity.getResources().getString(R.string.stream_not_started), Snackbar.LENGTH_LONG).show();
                                                                   }
                                                               });
                                        triggerStopRecording();
                                    }
                                }
                                }.execute(RTMP_BASE_URL + streamName);
                        } else {
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(activity, "Your previous broadcast still sends packets due to slow internet speed", Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    } else {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //  Snackbar.make(mRootView, R.string.oopps_shouldnt_happen, Snackbar.LENGTH_LONG).show();
                                Toast.makeText(activity, "Oopps this shouldn\\'t be happened.", Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                }
            }
            else {
                if (eventType.equals("2")) { // Event 2 handling

                    if (mLiveVideoBroadcaster != null) {
                        if (cameraID.equals("1")) {
                            mLiveVideoBroadcaster.changeCameraWithID(1);
                        } else {
                            mLiveVideoBroadcaster.changeCameraWithID(0);
                        }

                    }
                } else {    // Event 3 handling
                    // triggerStopRecording();
                }
            }
    }


    @Override
    public void onLocationChanged(Location location) {

        if (location!=null){
            if(speedDataQueueList.size() >= speedDataArrayLimit) {
                speedDataQueueList.remove(0);
            }  else {
                speedDataQueueList.add(location);
            }

            int speed=(int) ((location.getSpeed() * 3600) / 1000);
            driving_speed_value.setText("Driving speed: " + speed +" km/h");
            //driving_speed_value.setText("Diving speed: " + location.getSpeed() +" m/s");

            if(speedDataQueueList.size() > 1) {
                int range = 2;
                if(speedDataQueueList.size() >= 5){
                    range = 5;
                } else {
                    range = speedDataQueueList.size();
                }

                excessiveaccelerationOccured = false;
                excessivedecelerationOccured = false;

                Location curspeedValue = speedDataQueueList.get(speedDataQueueList.size() - 1); // latest value
                // Checking till last 5 values to see if excessive acceleration/ hard breaking has occured at any point
                for (int i = speedDataQueueList.size() - 2; i >= (speedDataQueueList.size() - range); i--) {
                    Location initialValue = speedDataQueueList.get(i);
                    double initialvelocity = initialValue.getSpeed();
                    double finalvelocity = curspeedValue.getSpeed();
                    long initialtime = (initialValue.getTime() / 1000);
                    long finaltime = (curspeedValue.getTime() / 1000);

                    AccelerationPojo accelerationPojo = new AccelerationPojo(initialvelocity, finalvelocity, initialtime, finaltime);
                    double curacceleration = accelerationPojo.calculateAcceleration();
                    double deltat = (finaltime - initialtime);
                    if(curacceleration < 0){    // deceleration
                        // if(Math.abs(curacceleration) >= (excessivedecelerationlimit)){
                        if(Math.abs(curacceleration) >= (excessivedecelerationlimit)){
                            excessivedecelerationOccured = true;
                            if(notification_view.getVisibility() == View.GONE){
                                notification_view.setText("Hard breaking detected");
                                notification_view.setVisibility(View.VISIBLE);
                                createhardBrakeOrExcessiveAccelRequest("1");
                                // store in db with timestamp, notify server
                            } else {
                                notification_view.setText("Hard breaking detected");
                            }
                            break;
                        }
                    } else {    // acceleration
                        // if(Math.abs(curacceleration) >= (excessiveaccelerationlimit)){
                        if(Math.abs(curacceleration) >= (excessiveaccelerationlimit)){
                            excessiveaccelerationOccured = true;
                            if(notification_view.getVisibility() == View.GONE){
                                notification_view.setText("Excessive acceleration detected");
                                notification_view.setVisibility(View.VISIBLE);
                                createhardBrakeOrExcessiveAccelRequest("2");
                                // store in db with timestamp, notify server
                            } else {
                                notification_view.setText("Excessive acceleration detected");
                            }
                            break;
                        }
                    }
                }

                if(!excessivedecelerationOccured && !excessiveaccelerationOccured){
                    notification_view.setVisibility(View.GONE);
                }
            }

//            speedDataQueue.add(1);
//            speedDataQueue.remove();
//            speedDataQueue.poll();
//            speedDataQueue.size();
            /*
            if (speed > SpeedLimit) {
                if (excessiveacceltnOccured.equals(false)) {
                    excessiveacceltnOccured = true;

                    Toast.makeText(LiveVideoBroadcasterActivity.this, "Speed Limit exceeded : " + speed, Toast.LENGTH_SHORT).show();

                    hardbreakOrExcessiveAccelApi(streamNameRegisterd, "1");

                    mihndler.postDelayed(mirRunnable = new Runnable() {
                        public void run() {

                            excessiveacceltnOccured = false;
                            Log.d("exicutedddd", "now");
                            //mihndler.postDelayed(mirRunnable, miDelay);
                        }
                    }, miDelay);
                }
            } */
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(getApplicationContext(), "Please turn on your GPS", Toast.LENGTH_SHORT).show();
    }

    //    location accesspermisssion
    private boolean isLocationEnabled(Context mContext) {

        LocationManager locationManager = (LocationManager)
                mContext.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public void createhardBrakeOrExcessiveAccelRequest(String eventType){

        File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).toString());
        if(!folder.exists()){
            folder.mkdir();
        }

        File video_file = new File(folder, "empty_file.mp4");
        // Map is used to multipart the file using okhttp3.RequestBody    File file = new File(mediaPath);

        pref = getApplicationContext().getSharedPreferences("DataStore", MODE_PRIVATE);
        String authcode = pref.getString("authcode", "");
        String deviceid = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
        long epoch = (System.currentTimeMillis() / 1000);
        String epochStr = String.valueOf(epoch);
        String latlng = "";
        if(speedDataQueueList != null){
            if(speedDataQueueList.size() > 0){
                Location location = speedDataQueueList.get(speedDataQueueList.size() - 1);
                String lat = String.valueOf(location.getLatitude());
                String lng = String.valueOf(location.getLongitude());
                latlng = lat + "," + lng;
            }
        }

        hardbreakOrExcessiveAccelApi(deviceid, authcode, latlng, eventType, epochStr, video_file);
    }


    public void hardbreakOrExcessiveAccelApi(String deviceId, String authcode, String latlng, String eventType, String epochStr, File video_file) {

        // Parsing any Media type file
        RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), video_file);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("video", video_file.getName(), requestBody);
        RequestBody video = RequestBody.create(MediaType.parse("text/plain"), video_file.getName());

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        /*
        Call<SendVideoResponse> call = apiService.getSendVideoResponse(authcode, deviceId, latlng, eventType, epochStr, fileToUpload);
        call.enqueue(new Callback<SendVideoResponse>() {
            @Override
            public void onResponse(Call<SendVideoResponse> call, Response<SendVideoResponse> response) {
                int statusCode = response.code();

                if (statusCode == 200) {
                    if (response.body().getStatus().equalsIgnoreCase("Success")) {
                        Log.d("SendVideo", "Success");
                    }
                    else {
                        Log.d("SendVideo", "Error");
                    }
                }
                else {
                    Log.d("SendVideo", "Request Failed");
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("SendVideo", "SendVideo failure");
            }
        }); */
    }
}