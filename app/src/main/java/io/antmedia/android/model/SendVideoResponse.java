package io.antmedia.android.model;

import com.google.gson.annotations.SerializedName;

public class SendVideoResponse {

    @SerializedName("status")
    public String status;

    @SerializedName("result")
    public String result;

    @SerializedName("url")
    public String url;

    @SerializedName("message")
    public String message;


    public SendVideoResponse(String status, String result, String url, String message) {
        this.status = status;
        this.result = result;
        this.url = url;
        this.message = message;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
