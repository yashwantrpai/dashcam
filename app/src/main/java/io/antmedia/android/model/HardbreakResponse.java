package io.antmedia.android.model;

import com.google.gson.annotations.SerializedName;

public class HardbreakResponse {



    @SerializedName("status")
    public String resStatus;

    @SerializedName("strmName")
    public String strmName;

    @SerializedName("msgText")
    public String msgText;


    public HardbreakResponse(String stat, String strmName, String msgText) {
        this.resStatus = stat;
        this.strmName = strmName;
        this.msgText = msgText;
    }



    public String getRStatus() {
        return resStatus;
    }
    public void setRStatus(String resStatus) {
        this.resStatus = resStatus;
    }

    public String getRstrmName() {
        return strmName;
    }
    public void setRstrmName(String resStatus) {
        this.strmName = strmName;
    }

    public String getRmsgTxt() {
        return msgText;
    }
    public void setRmsgTxt(String msgText) {
        this.msgText = msgText;
    }


}
