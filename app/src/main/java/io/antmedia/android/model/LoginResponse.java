package io.antmedia.android.model;

import com.google.gson.annotations.SerializedName;

public class LoginResponse {

    @SerializedName("status")
    public String status;

    @SerializedName("authcode")
    public String authcode;

    @SerializedName("message")
    public String message;


    public LoginResponse(String status, String authcode, String message) {
        this.status = status;
        this.authcode = authcode;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAuthcode() {
        return authcode;
    }

    public void setAuthcoden(String authcode) {
        this.authcode = authcode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
