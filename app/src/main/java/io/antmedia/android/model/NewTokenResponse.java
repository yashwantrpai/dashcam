package io.antmedia.android.model;

import com.google.gson.annotations.SerializedName;

public class NewTokenResponse {

    @SerializedName("status")
    public String status;

    @SerializedName("firebasetoken")
    public String firebasetoken;

    @SerializedName("message")
    public String message;


    public NewTokenResponse(String status, String firebasetoken, String message) {
        this.status = status;
        this.firebasetoken = firebasetoken;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFirebasetoken() {
        return firebasetoken;
    }

    public void setFirebasetoken(String firebasetoken) {
        this.firebasetoken = firebasetoken;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
