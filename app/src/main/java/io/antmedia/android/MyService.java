package io.antmedia.android;

import android.app.Service;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

import io.antmedia.android.liveVideoBroadcaster.CaptureActivity;

public class MyService extends Service {

    int counter = 0;
    public static boolean isRunning = false;
    public String TAG = "MyService";
    public static CountDownTimer timer;
    public MyService() {
        counter = 0;
        isRunning = false;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        isRunning = true;
        Log.d(TAG, "Service started");

        timer = new CountDownTimer(86400000, 1000) {
                public void onTick(long millisUntilFinished) {
                    // startRecording();
                    counter++;
                    Log.d(TAG, "Counter: " + counter);
                    if (counter > 10) {
                        if (CaptureActivity.recording && !CaptureActivity.isHardBrakeExcessAccelEvent) {
                            Log.d(TAG, "Video reset");
                            resetVideo();
                        }
                    }
                }

                public void onFinish() {
                    stopRecording();
                    isRunning = false;
                    counter = 0;
                    Log.d(TAG, "Counter: " + counter);
                    stopSelf();
                }
            };
        timer.start();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return null;
    }

    public void resetVideo(){
        stopRecording();
        removeVideo();
        startRecording();
        counter = 0;
    }

    public void removeVideo(){
        File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).toString());
        if(folder.exists()){
            File video_file = new File(folder, "streaming.mp4");
            video_file.delete();
        }
    }

    public void startRecording(){
        try {
            CaptureActivity.initRecorder();
            CaptureActivity.prepareRecorder();
            CaptureActivity.recording = true;
            CaptureActivity.recorder.start();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public void stopRecording(){
        try {
            CaptureActivity.recording = false;
            CaptureActivity.releaseMediaRecorder();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        timer.onFinish();
        timer.cancel();
    }
}
