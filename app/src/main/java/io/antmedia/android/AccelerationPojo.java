package io.antmedia.android;

public class AccelerationPojo {

    double initialvelocity;
    double finalvelocity;
    long initialtimesec;
    long finaltimesec;

    public AccelerationPojo() {
        this.initialvelocity = 0.0;
        this.finalvelocity = 0.0;
        this.initialtimesec = 0;
        this.finaltimesec = 0;
    }

    public AccelerationPojo(double initialvelocity, double finalvelocity, long initialtimesec, long finaltimesec) {
        this.initialvelocity = initialvelocity;
        this.finalvelocity = finalvelocity;
        this.initialtimesec = initialtimesec;
        this.finaltimesec = finaltimesec;
    }

    public double calculateAcceleration(){  // in m/s2
        double deltav = (finalvelocity - initialvelocity);
        double deltat = (double) (finaltimesec - initialtimesec);

        double acceleration = (deltav / deltat);

        return acceleration;
    }
}
