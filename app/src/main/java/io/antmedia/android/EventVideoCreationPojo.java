package io.antmedia.android;

public class EventVideoCreationPojo {

    VideoStreamPojo startVideo;
    String startTime;
    String startDuration;
    VideoStreamPojo endVideo;
    String endTime;
    String endDuration;
    String event;

    public EventVideoCreationPojo() {
        startVideo = null;
        startTime = "";
        startDuration = "";
        endVideo = null;
        endTime = "";
        endDuration = "";
        event = "";
    }

    public EventVideoCreationPojo(VideoStreamPojo startVideo, String startTime, String startDuration, VideoStreamPojo endVideo, String endTime, String endDuration, String event) {
        this.startVideo.setVideoFile(startVideo.getVideoFile());
        this.startVideo.setStartTime(startVideo.getStartTime());
        this.startVideo.setEndTime(startVideo.getEndTime());
        this.startDuration = startDuration;

        this.endVideo.setVideoFile(endVideo.getVideoFile());
        this.endVideo.setStartTime(endVideo.getStartTime());
        this.endVideo.setEndTime(endVideo.getEndTime());
        this.endDuration = endDuration;

        this.startTime = startTime;
        this.endTime = endTime;
        this.event = event;
    }

    public VideoStreamPojo getStartVideo() {
        return startVideo;
    }

    public void setStartVideo(VideoStreamPojo startVideo) {
        this.startVideo.setVideoFile(startVideo.getVideoFile());
        this.startVideo.setStartTime(startVideo.getStartTime());
        this.startVideo.setEndTime(startVideo.getEndTime());
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public VideoStreamPojo getEndVideo() {
        return endVideo;
    }

    public void setEndVideo(VideoStreamPojo endVideo) {
        this.endVideo.setVideoFile(endVideo.getVideoFile());
        this.endVideo.setStartTime(endVideo.getStartTime());
        this.endVideo.setEndTime(endVideo.getEndTime());
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getStartDuration() {
        return startDuration;
    }

    public void setStartDuration(String startDuration) {
        this.startDuration = startDuration;
    }

    public String getEndDuration() {
        return endDuration;
    }

    public void setEndDuration(String endDuration) {
        this.endDuration = endDuration;
    }
}
