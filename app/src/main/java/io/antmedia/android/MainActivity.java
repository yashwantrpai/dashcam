package io.antmedia.android;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaRecorder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.common.api.Api;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

import io.antmedia.android.broadcaster.LiveVideoBroadcaster;
import io.antmedia.android.liveVideoBroadcaster.*;
import io.antmedia.android.liveVideoBroadcaster.R;
import io.antmedia.android.model.HardbreakResponse;
import io.antmedia.android.model.LoginResponse;
import io.antmedia.android.model.NewTokenResponse;
import io.antmedia.android.model.SendVideoResponse;
import io.antmedia.android.rest.ApiClient;
import io.antmedia.android.rest.ApiInterface;
import android.provider.Settings.Secure;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    // public static final String RTMP_BASE_URL = "rtmp://23.239.28.100/LiveApp/";
    public static final String RTMP_BASE_URL = "rtmp://50.116.13.48/LiveApp/";
    // public static final String RTMP_BASE_URL = "rtmp://192.168.88.130/LiveApp/";
    // public static final String RTMP_BASE_URL = "rtmp://test.antmedia.io/LiveApp/";
    // public static final String RTMP_BASE_URL = "rtmp://18.232.155.25/LiveApp/";  // aws

    public static MainActivity myInstance;
    private static final String TAG = MainActivity.class.getSimpleName();

    private static EditText username_edittext;
    private static TextView login_btn_text, user_pass_edit_text, test_firebase, test_video;
    private static ProgressBar spinner;
    public static Activity activity;
    private static final int REQUEST_PERMISSIONS = 10;
    public String[] permissions;
    public ArrayList<String> permissionsList;
    public boolean permissionsGranted = false;
    private static String username = "", password = "", deviceid = "", token = "";
    public static SharedPreferences pref;
    public static SharedPreferences.Editor editor;
    public static AlertDialog GpsAlert;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(io.antmedia.android.liveVideoBroadcaster.R.layout.activity_main);

        myInstance = this;
        activity = this;
        username_edittext = (EditText) findViewById(R.id.user_name_edit_text);
        user_pass_edit_text = (EditText) findViewById(R.id.user_pass_edit_text);
        login_btn_text = (TextView) findViewById(R.id.login_btn_text);
        // test_firebase = (TextView) findViewById(R.id.test_firebase);
        test_video = (TextView) findViewById(R.id.test_video);
        permissions = new String[]{};
        permissionsList = new ArrayList<>();

        spinner = (ProgressBar) findViewById(R.id.progressBar1);
        pref = activity.getSharedPreferences("DataStore", MODE_PRIVATE);
        editor = pref.edit();

        if(!TextUtils.isEmpty(pref.getString("authcode", ""))){
            // Intent captureIntent = new Intent(MainActivity.this, CaptureActivity.class);
            // activity.startActivity(captureIntent);
            // finish();
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(Manifest.permission.CAMERA);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(Manifest.permission.RECORD_AUDIO);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        permissions = permissionsList.toArray(permissions);

        if (permissions.length > 0) {
            ActivityCompat.requestPermissions(activity, permissions, REQUEST_PERMISSIONS);
        } else {
            permissionsGranted = true;
        }

        login_btn_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openVideoBroadcaster();
            }
        });


        test_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hardbrakeOrExcessiveAccelApi();
            }
        });
    }

    public void hardbrakeOrExcessiveAccelApi() {

        File video_file = getSavedPath();
        String authcode = "393840380";
        String deviceId = "3895304580";
        String latlng = "3458930458930";
        String eventType = "2";
        String epochStr = "28340198";
        String fileName = "HB";

        // Parsing any Media type file
        RequestBody requestBody = RequestBody.create(MediaType.parse("*/*"), video_file);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("video", fileName, requestBody);
        RequestBody video = RequestBody.create(MediaType.parse("text/plain"), video_file.getName());

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<SendVideoResponse> call = apiService.getSendVideoResponse(authcode, deviceId, latlng, eventType, epochStr, fileToUpload);
        // Call<SendVideoResponse> call = apiService.getSendVideoResponse(authcode, deviceId, latlng, eventType, epochStr);
        call.enqueue(new Callback<SendVideoResponse>() {
            @Override
            public void onResponse(Call<SendVideoResponse> call, Response<SendVideoResponse> response) {
                int statusCode = response.code();

                if (statusCode == 200) {
                    if (response.body().getStatus().equalsIgnoreCase("Success")) {
                        Log.d("SendVideo", "Success");
                        Toast.makeText(activity, "Video uploaded", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Log.d("SendVideo", "Error");
                        Toast.makeText(activity, "Video upload failed", Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Log.d("SendVideo", "Request Failed");
                    Toast.makeText(activity, "Video upload failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.d("SendVideo", "SendVideo failure");
                Toast.makeText(activity, "Video upload failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public File getSavedPath(){
        File folder = getFolderPath();
        File video_file = new File(folder, "saved_video_out.mp4");

        return video_file;
    }

    public static File getFolderPath(){
        File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).toString());
        if(!folder.exists()){
            folder.mkdir();
        }
        return folder;
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!isLocationEnabled(this)) {
            //show dialog if Location Services is not enabled
            GPSalert();
        } else {
            if(GpsAlert != null &&  GpsAlert.isShowing()){
                GpsAlert.dismiss();
            }
        }
    }

    public void openVideoBroadcaster() {

        username = username_edittext.getText().toString();
        password = user_pass_edit_text.getText().toString();

        if (TextUtils.isEmpty(username)) {
            Toast.makeText(MainActivity.this,"Please enter username", Toast.LENGTH_LONG).show();
        } else if (TextUtils.isEmpty(password)) {
            Toast.makeText(MainActivity.this,"Please enter password", Toast.LENGTH_LONG).show();
        }
        else {
            if (permissionsGranted) {
                deviceid = Secure.getString(activity.getContentResolver(), Secure.ANDROID_ID);

                FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(MainActivity.this, new OnSuccessListener<InstanceIdResult>() {
                    @Override
                    public void onSuccess(InstanceIdResult instanceIdResult) {
                        String token = instanceIdResult.getToken();
                        Log.d("FCM_TOKEN", token);

                        loginApi(username, password, deviceid, token);

                    }
                });
            } else {
                appWontWorkWithoutPermissionsAlert();
            }
        }
    }

    public void reloadScreen(final String body) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.d("body", body);
//                txt.setText(body);
            }
        });
        Log.v("responce","body"+body);

        Intent i = new Intent(this, LiveVideoBroadcasterActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        myInstance=null;
    }

    public static void loginApi(String usernameVal, String passVal, String deviceId, String tokenVal) {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        spinner.setVisibility(View.VISIBLE);
        login_btn_text.setEnabled(false);

        Call<LoginResponse> call = apiService.getLoginCheckResponse(usernameVal, passVal, deviceId, tokenVal);  //apiService.getLoginCheckResponse(RTMP_BASE_URL);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                int statusCode = response.code();
                //List<Movie> movies = response.body().getResults();

                spinner.setVisibility(View.GONE);
                login_btn_text.setEnabled(true);

                if (statusCode == 200) {
                    if (response.body().getStatus().equalsIgnoreCase("Success")) {
                        // if (ContextCompat.checkSelfPermission(myInstance, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                          //  if (ActivityCompat.shouldShowRequestPermissionRationale(myInstance, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                          //      ActivityCompat.requestPermissions(myInstance, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);
                          //  }
                        // } else {

                        String authcode = response.body().getAuthcode();
                        editor.putString("authcode", authcode);
                        editor.commit();

                        // Intent i = new Intent(activity, LiveVideoBroadcasterActivity.class);
                        // i.putExtra("strmName", username);
                        // Intent i = new Intent(activity, CaptureActivity.class);
                        // activity.startActivity(i);
                        // activity.finish();
                        // }
                    }
                    else {
                        LoginFailedAlert();
                    }
                }
                else {
                    Toast.makeText(activity,"Something went wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                // Log error here since request failed

                spinner.setVisibility(View.GONE);
                login_btn_text.setEnabled(true);
                Log.e(TAG, t.toString());
                Toast.makeText(activity,"Please check your internet connection", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_PERMISSIONS: {
                permissionsGranted = true;
                for(int result:grantResults) {
                    if(result != PackageManager.PERMISSION_GRANTED){
                        permissionsGranted = false;
                    }
                }

                if(!permissionsGranted){
                    appWontWorkWithoutPermissionsAlert();
                } else {
                    if (!isLocationEnabled(this)) {

                        //show dialog if Location Services is not enabled

                    }
                }
            }
        }
    }

    public void GPSalert(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.gps_not_found_title);  // GPS not found
        builder.setMessage(R.string.gps_not_found_message); // Want to enable?
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                myInstance.startActivity(intent);
            }
        });
        GpsAlert = builder.create();
        GpsAlert.show();
    }

    public void appWontWorkWithoutPermissionsAlert(){
        new android.app.AlertDialog.Builder(activity)
                .setTitle(R.string.permission)
                .setMessage(getString(R.string.app_doesnot_work_without_permissions))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            intent.setData(Uri.parse("package:" + getApplicationContext().getPackageName()));
                            startActivity(intent);

                        } catch (ActivityNotFoundException e) {
                            Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS);
                            startActivity(intent);
                        }
                    }
                })
                .show();
    }

    public static void LoginFailedAlert(){
        new android.app.AlertDialog.Builder(activity)
                .setTitle(R.string.login_failed)
                .setMessage(activity.getString(R.string.login_failed_text))
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    //    location accesspermisssion
    private boolean isLocationEnabled(Context mContext) {

        LocationManager locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private void sendTokenBackEnd(String newtoken) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        pref = getApplicationContext().getSharedPreferences("DataStore", MODE_PRIVATE);
        String authcode = pref.getString("authcode", "");

        Call<NewTokenResponse> call = apiService.getNewTokenResponse(newtoken, authcode);
        call.enqueue(new Callback<NewTokenResponse>() {
            @Override
            public void onResponse(Call<NewTokenResponse> call, Response<NewTokenResponse> response) {
                int statusCode = response.code();
                //List<Movie> movies = response.body().getResults();

                if (statusCode == 200) {
                    if (response.body().getStatus().equals("Success")) {
                        Log.d(TAG,"Token updated in server");
                    } else {
                        Log.d(TAG, "Error with token updation");
                    }
                }
                else {
                    Log.d(TAG,"Something went wrong");
                }
            }

            @Override
            public void onFailure(Call<NewTokenResponse> call, Throwable t) {
                Log.d(TAG,"Something went wrong");
            }
        });
    }
}
