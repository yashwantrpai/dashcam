package io.antmedia.android;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;

import com.crashlytics.android.Crashlytics;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;

import io.fabric.sdk.android.Fabric;


public class App extends Application {

    public static Context appContext;
    public static FFmpeg ffmpeg;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        appContext = this;
        ffmpeg = FFmpeg.getInstance(appContext);
        loadffmpegbinary();

    }

    public static Context getContext(){
        return appContext;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();

    }

    public static FFmpeg getFfmpeg() {
        return ffmpeg;
    }

    public void setFfmpeg(FFmpeg ffmpeg) {
        this.ffmpeg = ffmpeg;
    }

    public void loadffmpegbinary(){
        try {
            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {

                @Override
                public void onStart() {}

                @Override
                public void onFailure() {}

                @Override
                public void onSuccess() {}

                @Override
                public void onFinish() {}
            });
        } catch (FFmpegNotSupportedException e) {
            // Handle if FFmpeg is not supported by device
            e.printStackTrace();
        }
    }
}