package io.antmedia.android.fcm;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import io.antmedia.android.App;
import io.antmedia.android.MYAppContext;
import io.antmedia.android.MainActivity;
import io.antmedia.android.broadcaster.LiveVideoBroadcaster;
import io.antmedia.android.liveVideoBroadcaster.CaptureActivity;
import io.antmedia.android.liveVideoBroadcaster.LiveVideoBroadcasterActivity;
import io.antmedia.android.liveVideoBroadcaster.R;
import io.antmedia.android.model.HardbreakResponse;
import io.antmedia.android.model.NewTokenResponse;
import io.antmedia.android.rest.ApiClient;
import io.antmedia.android.rest.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class MYFirebaseMessagingService extends FirebaseMessagingService {

    public SharedPreferences pref;
    public SharedPreferences.Editor editor;
    public String TAG = "Firebase newtoken";
    public static Map<String, String> jsData = null;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.d("responce", "From: " + remoteMessage.getFrom());
        pref = getApplicationContext().getSharedPreferences("DataStore", MODE_PRIVATE);
        editor = pref.edit();

        if(!CaptureActivity.isHardBrakeExcessAccelEventUpload) {
            //  Check if message contains a data payload.
            if (remoteMessage.getData().size() > 0) {
                Log.d("responce", "Message data payload: " + remoteMessage.getData());

//              remoteMessage.getData().get("Key");
                jsData = remoteMessage.getData();
                editor.putString("streamName", jsData.get("streamName"));
                editor.putString("eventType", jsData.get("eventType"));
                editor.putString("cameratype", jsData.get("cameratype"));
                editor.commit();
                //String value1 = jsData.get("key1");

                if (jsData.get("eventType").equals("3")) {
                    if (!CaptureActivity.isCaptureActivityRunning) {
                        if (!TextUtils.isEmpty(pref.getString("authcode", ""))) {
                            //if (LiveVideoBroadcasterActivity.isLiveBroadcastActivityRunning) {
                              // LiveVideoBroadcasterActivity.activity.finish();
                            //}
                            Intent i = new Intent();
                            i.setClass(this, CaptureActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                            // LiveVideoBroadcasterActivity.activity.finish();
                            android.os.Process.killProcess(android.os.Process.myPid());
                        }
                    }
                } else {
                    if (!LiveVideoBroadcasterActivity.isLiveBroadcastActivityRunning) {
                        if (!TextUtils.isEmpty(pref.getString("authcode", ""))) {
                            // if (CaptureActivity.isCaptureActivityRunning) {
                               // CaptureActivity.activity.finish();
                            // }
                            Intent i = new Intent();
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            i.setClass(this, LiveVideoBroadcasterActivity.class);
                            startActivity(i);
                            // CaptureActivity.activity.finish();
                            android.os.Process.killProcess(android.os.Process.myPid());
                        }
                    } else {
                        LiveVideoBroadcasterActivity.notificationCustomAction();
                    }
                }

//            try {
//                JSONObject json = new JSONObject(remoteMessage.getData().toString());
//                LiveVideoBroadcasterActivity.brdInstance.notificationCustomAction();
//
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
            }

//        // Check if message contains a notification payload.
//        if (remoteMessage.getNotification() != null) {
//            Log.d("responce", "Message Notification Body: " + remoteMessage.getNotification().getBody());
//            // MYAppContext.reLoadScreen(remoteMessage.getNotification().getBody());
////            MYAppContext.reLoadScreen("hellom");
////            MainActivity.myInstance.reloadScreen(remoteMessage.getNotification().getBody());
//
//            LiveVideoBroadcasterActivity.brdInstance.notificationCustomAction();
//        }
        }
    }

    public static Map<String, String> getJsData(){
        return jsData;
    }

    public static void resetJsData(){
        jsData = null;
    }

    @Override
    public void onNewToken(String newtoken) {
        super.onNewToken(newtoken);
        Log.v("responce","new Token "+newtoken);
        sendTokenBackEnd(newtoken);
    }

    private void sendTokenBackEnd(String newtoken) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        pref = getApplicationContext().getSharedPreferences("DataStore", MODE_PRIVATE);
        String authcode = pref.getString("authcode", "");

        Call<NewTokenResponse> call = apiService.getNewTokenResponse(newtoken, authcode);
        call.enqueue(new Callback<NewTokenResponse>() {
            @Override
            public void onResponse(Call<NewTokenResponse> call, Response<NewTokenResponse> response) {
                int statusCode = response.code();
                //List<Movie> movies = response.body().getResults();

                if (statusCode == 200) {
                    if (response.body().getStatus().equals("true")) {
                        Log.d(TAG,"Token updated in server");
                    } else {
                        Log.d(TAG, "Error with token updation");
                    }
                }
                else {
                    Log.d(TAG,"Something went wrong");
                }
            }

            @Override
            public void onFailure(Call<NewTokenResponse> call, Throwable t) {
                Log.d(TAG,"Something went wrong");
            }
        });
    }
}
