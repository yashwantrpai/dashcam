package io.antmedia.android;

import android.app.Application;
import android.content.Context;
import android.os.Handler;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;

import io.antmedia.android.MainActivity;

public class MYAppContext extends Application {

    private static MYAppContext mInstance;
    static Context context;

    public static Context getAppContext() {
        return context;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        context = getApplicationContext();
    }

    static { AppCompatDelegate.setCompatVectorFromResourcesEnabled(true); }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    public static synchronized MYAppContext getInstance() {
        return mInstance;
    }

    static Handler handler = new Handler();

    public static void reLoadScreen(final String body) {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() { // This thread runs in the UI
                    @Override
                    public void run() {
//                        Update UI
                        Log.d("body", body);
                        //MainActivity.myInstance.reloadScreen(body);

                    }
                });
            }
        };
        new Thread(runnable).start();

    }
}
