package io.antmedia.android;

import com.android.midhun.firsample.MyIntentService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Util {

    public static SimpleDateFormat dateTimeFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
    public static SimpleDateFormat fileNameFormat = new SimpleDateFormat("MMddyyyyHHmmss");

    public static double tokmph(double mps){
        double conversionfactor = 3.6;
        return (mps * conversionfactor);
    }

    public static double tomps(double kmph){
        double conversionfactor = 0.277778;
        return (kmph * conversionfactor);
    }

    public static EventVideoCreationPojo createEventVideo(String eventStartTime, String eventEndTime, String event) {
        EventVideoCreationPojo eventVideoCreationPojo = new EventVideoCreationPojo();

        VideoStreamPojo savedPojo = MyIntentService.getSavedPojo();
        VideoStreamPojo streamingPojo = MyIntentService.getStreamingPojo();

        try {
            Date eventStartDateTime = dateTimeFormat.parse(eventStartTime);
            Date eventEndDateTime = dateTimeFormat.parse(eventEndTime);

            Date savedStartDateTime = dateTimeFormat.parse(savedPojo.getStartTime());
            Date savedEndDateTime = dateTimeFormat.parse(savedPojo.getEndTime());
            Date streamingStartDateTime = dateTimeFormat.parse(streamingPojo.getStartTime());
            Date streamingEndDateTime = dateTimeFormat.parse(streamingPojo.getEndTime());

            // Fill EventVideoCreationPojo
            // Find start video, fill startVideo, startTime
            if((savedStartDateTime.before(eventStartDateTime) || savedStartDateTime.equals(eventStartDateTime) || savedStartDateTime.after(eventStartDateTime))){
                // Start datetime
                if(savedStartDateTime.after(eventStartDateTime)){
                    eventVideoCreationPojo.setStartTime(savedPojo.getStartTime());
                } else {
                    eventVideoCreationPojo.setStartTime(eventStartTime);
                }

                // Start video
                if(eventStartDateTime.before(savedEndDateTime)){
                    eventVideoCreationPojo.setStartVideo(savedPojo); // Startvideo is saved
                } else if(eventStartDateTime.equals(savedEndDateTime) || eventStartDateTime.after(savedEndDateTime)){ // moving out of saved
                     if(streamingStartDateTime.before(eventStartDateTime) || streamingStartDateTime.equals(eventStartDateTime) || streamingStartDateTime.after(eventStartDateTime)){
                         if(streamingStartDateTime.equals(eventStartDateTime) || streamingStartDateTime.after(eventStartDateTime)){
                             eventVideoCreationPojo.setStartTime(streamingPojo.getStartTime());
                         }
                     }
                }
            }

            // Find end video, fill endVideo, endTime

            // return VideoStreamPojo value to CaptureActivity.createhardBrakeOrExcessiveAccelRequest()
            eventVideoCreationPojo.setEvent(event);
            return eventVideoCreationPojo;
        } catch (ParseException e) {
            e.printStackTrace();

        }

        return eventVideoCreationPojo;
    }

    public static String eventFull(String evt){
        switch (evt){
            case "1":
                return "Hard Braking";

            case "2":
                return "Excess Acceleration";
        }
        return "";
    }

    public static String eventShort(String evt){
        switch (evt){
            case "1":
                return "HB";

            case "2":
                return "EXA";
        }
        return "";
    }
}
