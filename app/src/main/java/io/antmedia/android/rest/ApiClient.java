package io.antmedia.android.rest;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiClient {

    //http://23.239.28.100/mydealership/ganntChart_dhara/save.php?username=%22abhilash534%22&&token=%22624637836873658%22
    public static final String BASE_URL = "http://23.239.28.100/mydealership/dashcamlib/";
    // 50.116.13.48
    // public static final String BASE_URL = "http://50.116.13.48/dashcamlib/";

    private static Retrofit retrofit = null;

    public static Retrofit getClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
