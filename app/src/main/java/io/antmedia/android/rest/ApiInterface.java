package io.antmedia.android.rest;

import io.antmedia.android.model.HardbreakResponse;
import io.antmedia.android.model.LoginResponse;
import io.antmedia.android.model.LogoutResponse;
import io.antmedia.android.model.NewTokenResponse;
import io.antmedia.android.model.SendVideoResponse;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface ApiInterface {
//    @GET("save.php")
//    Call<LoginResponse> getLoginCheckResponse(@Query("api_key") String apiKey);

    @GET("api.php?action=login")
    Call<LoginResponse> getLoginCheckResponse(@Query("user") String userName, @Query("pass") String password, @Query("deviceid") String deviceid, @Query("firebasetoken") String firebasetoken);

    @GET("hardbreakingsave.php")
    Call<HardbreakResponse> getHardbreakOrExcessiveAccelResponse(@Query("streamName") String streamName, @Query("eventID") String subEventID);

    @GET("api.php?action=newtoken")
    Call<NewTokenResponse> getNewTokenResponse(@Query("firebasetoken") String firebasetoken, @Query("auth") String auth);

    @Multipart
    @POST("api.php?action=sendvideo")
    Call<SendVideoResponse> getSendVideoResponse(@Query("authcode") String authcode, @Query("deviceid") String deviceid, @Query("latlong") String latlong, @Query("eventtype") String eventtype, @Query("eventtime") String eventtime, @Part MultipartBody.Part video1);
    // Call<SendVideoResponse> getSendVideoResponse(@Query("authcode") String authcode, @Query("deviceid") String deviceid, @Query("latlong") String latlong, @Query("eventtype") String eventtype, @Query("eventtime") String eventtime);

    @GET("api.php?action=logout")
    Call<LogoutResponse> getLogoutResponse(@Query("authcode") String authcode);
}
