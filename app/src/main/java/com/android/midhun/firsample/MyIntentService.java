package com.android.midhun.firsample;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.os.CountDownTimer;
import android.os.Environment;
import android.util.Log;

import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import io.antmedia.android.App;
import io.antmedia.android.VideoStreamPojo;
import io.antmedia.android.ffmpegUtil;
import io.antmedia.android.liveVideoBroadcaster.CaptureActivity;
import io.antmedia.android.liveVideoBroadcaster.R;

import static io.antmedia.android.Util.dateTimeFormat;
import static io.antmedia.android.liveVideoBroadcaster.CaptureActivity.getFolderPath;


public class MyIntentService extends IntentService {

    int counter = 0;
    public static boolean isRunning = false;
    public String TAG = "MyIntentService";
    public static VideoStreamPojo savedPojo;
    public static VideoStreamPojo streamingPojo;

    public MyIntentService() {
        super("MyIntentService");
        counter = 0;
        isRunning = false;
    }

    public static VideoStreamPojo getSavedPojo() {
        return savedPojo;
    }

    public static VideoStreamPojo getStreamingPojo() {
        return streamingPojo;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            savedPojo = new VideoStreamPojo();
            streamingPojo = new VideoStreamPojo();

                isRunning = true;
                Log.d(TAG, "Service started");

                String startDateTime = dateTimeFormat.format(new Date());
                streamingPojo.setStartTime(startDateTime);
                streamingPojo.setVideoFile(getStreamingVideo());

                new CountDownTimer(86400000, 1000) {
                    public void onTick(long millisUntilFinished) {
                        // startRecording();
                        counter++;
                        Log.d(TAG, "Counter: " + counter);
                        if (counter > 30) {
                            if (CaptureActivity.recording && !CaptureActivity.isHardBrakeExcessAccelEvent) {
                                Log.d(TAG, "Video reset");
                                resetVideo();
                            }
                        }
                    }
                    public void onFinish() {
                        Log.d(TAG, "Counter: " + counter);
                    }
                }.start();
        }
    }

    public void resetVideo(){
        stopRecording();
        removeVideo();
        startRecording();
        counter = 0;
    }

    public void copyCmd(){
        String copy_cmd = getResources().getString(R.string.ffmpeg_copy_cmd);
        String videoIn = getFolderPath() + "/streaming.mp4";
        String videoOut = getFolderPath() + "/saved.mp4";
        copy_cmd = copy_cmd.replace("video_in", videoIn);
        copy_cmd = copy_cmd.replace("video_out", videoOut);
        String[] copy = ffmpegUtil.convertToCommandStringArray(copy_cmd);
        executeFFMPEGCommand(copy);
    }

    public void copyStreamingToSaved(){
        savedPojo = new VideoStreamPojo();
        savedPojo.setVideoFile(streamingPojo.getVideoFile());
        savedPojo.setStartTime(streamingPojo.getStartTime());
        savedPojo.setEndTime(streamingPojo.getEndTime());
        copyCmd(); // Copy streaming file to saved
    }

    public static File getStreamingVideo(){
        File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).toString());
        if(folder.exists()) {
            File streaming_video = new File(folder, "streaming.mp4");
            return streaming_video;
        }
        return null;
    }

    public static File getSavedVideo(){
        File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).toString());
        if(folder.exists()) {
            File saved_video = new File(folder, "saved.mp4");
            return saved_video;
        }
        return null;
    }

    public void removeVideo(){
        File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).toString());
        if(folder.exists()){
            File video_file = new File(folder, "streaming.mp4");
            video_file.delete();
        }
    }

    public void startRecording(){
        String startDateTime = dateTimeFormat.format(new Date());
        streamingPojo = new VideoStreamPojo();
        streamingPojo.setStartTime(startDateTime);
        streamingPojo.setVideoFile(getStreamingVideo());
        try {
            CaptureActivity.initRecorder();
            CaptureActivity.prepareRecorder();
            CaptureActivity.recording = true;
            CaptureActivity.recorder.start();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public void stopRecording(){
        try {
            CaptureActivity.recording = false;
            CaptureActivity.releaseMediaRecorder();
            String endDateTime = dateTimeFormat.format(new Date());
            streamingPojo.setEndTime(endDateTime);
            copyStreamingToSaved();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public void executeFFMPEGCommand(String[] cmd){
        try {
            // to execute "ffmpeg -version" command you just need to pass "-version"
            App.getFfmpeg().execute(cmd, new ExecuteBinaryResponseHandler() {

                @Override
                public void onStart() {
                    Log.d("ffmpeg", "onStart");
                }

                @Override
                public void onProgress(String message) {
                    Log.d("ffmpeg", "onProgress: "+message);
                }

                @Override
                public void onFailure(String message) {
                    Log.d("ffmpeg", "onFailure: "+message);
                }

                @Override
                public void onSuccess(String message) {
                    Log.d("ffmpeg", "onSuccess: "+message);
                }

                @Override
                public void onFinish() {
                    Log.d("ffmpeg", "onFinish");
                }
            });
        } catch (FFmpegCommandAlreadyRunningException e) {
            // Handle if FFmpeg is already running
            e.printStackTrace();
        }
    }
}
